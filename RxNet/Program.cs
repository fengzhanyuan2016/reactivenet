﻿using System;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Timers;

namespace RxNet
{
    class Program
    {
        static void Main(string[] args)
        {

            ObservableFactory factory = new ObservableFactory();
            var observable = factory.Create();
            Console.ReadKey();
            /*
            Action<string> onNext = x => Console.WriteLine(x);
            
            using (observable.Subscribe(i => onNext(i)))
            {
                Console.ReadKey();
            }

            */




            /*

            //create/generate 创建可观察对象
            var observable = Observable.Create<string>(x =>
            {
                var timer = new Timer();
                x.OnNext("⏰");
                x.OnNext("2");
                x.OnNext("3");
                x.OnNext("4");
                x.OnCompleted();
                return timer;
            });

            Observable.Generate<int, int>(0, i=>i<10,i => ++i,i=>i+9);

            using (observable.Subscribe(x => Console.WriteLine(x)))
            {
                Console.ReadKey();
            }

            //defer 延时创建可观察对象
            Observable.Defer<int>(() =>
            {
                return Observable.Empty<int>();
            });

            //Empty,Never,Throw
            Observable.Empty<int>();
            Observable.Never<int>();
            Observable.Throw<int>(new Exception());


            //From=ToObservable
            int[] items= { 0, 1, 2, 3, 4, 5 };
            items.ToObservable();

            //Interval
            Observable.Interval(TimeSpan.FromSeconds(2));


            //Return=Just 
            Observable.Return<string>("value");

            //Range
            Observable.Range(1,10);


            //Repeat
            Observable.Repeat(2, 5);

            //Start
            Observable.Start(()=> {
                Console.Write("Working away");
                for (int i = 0; i < 10; i++)
                {
                    Console.Write(".");
                }
            });

            //Timer  创建一个数据流：延迟指定时间发送一个值然后结束。
            Observable.Timer(TimeSpan.FromSeconds(1));

    */



        }

        public static void Println(int x)
        {
            Console.WriteLine(x);
        }
    }
}

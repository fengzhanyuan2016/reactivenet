﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Timers;

namespace RxNet
{
   public class ObservableFactory
    {
        public IObservable<string> Create()
        {
            var observable = Observable.Create<string>(x =>
            {
                var timer = new Timer();
                x.OnNext("⏰1");
                x.OnNext("⏰2");
                x.OnNext("⏰3");
                x.OnNext("⏰4");
                x.OnCompleted();
                return timer;
            });
            return observable;
        }
        


    }
}
